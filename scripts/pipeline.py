import os
from git import Repo


class PipelineWriter:

    @staticmethod
    def parent_job_template():
        parent_job_template = """
stages:
  - dev_deploy
  - prod_deploy

.base:
  image: registry.gitlab.com/tokenplace/infrastructure/build-image:db478d17
  script:
    - echo $SERVICE 
    - echo $ENV
  when: manual
"""
        return parent_job_template

    @staticmethod
    def child_pipeline_job_template(service):
        child_pipeline_job_template = f"""
{service}-dev-deploy:
  extends: .base
  stage: dev_deploy
  variables:
     SERVICE: {service}
     ENV: dev
{service}-prod-deploy:
  extends: .base
  stage: prod_deploy
  variables:
     SERVICE: {service}
     ENV: prod
"""
        return child_pipeline_job_template


def get_services():
    repo = Repo("./")
    commit = repo.head.commit
    services = []
    for file in commit.stats.files:
        if 'services' in file:
            services.append(file.split('/', 1)[1].split('.')[0].replace('/', '-'))
    return services


def generator():
    with open('service-jobs-gitlab-ci.yml', 'w+') as file:
        file.write(PipelineWriter.parent_job_template())
        for service in get_services():
            file.write(PipelineWriter.child_pipeline_job_template(service))


if __name__ == "__main__":
    generator()
